//Data structure for creating a dynamic action on an application card to open dialog modal
// We're defining a application card that has an action that is targeting a dialog that was defined in the app descriptor.
//https://developer.atlassian.com/cloud/stride/apis/document/nodes/applicationCard/
let actionCard = {
  version: 1,
  type: 'doc',
  content: [
    {
      type: 'applicationCard',
      attrs: {
        text: 'Application Card',
        actions: [
          {
            title: 'view dialog',
            target: {
              key: 'app-dialog'
            },
            parameters: {
              param1: 'Action from a Application Card'
            }
          }
        ],
        collapsible: true,
        title: {
          text: 'Sheldon Callahan updated a file: applicationCard.md',
          user: {
            icon: {
              url:
                'https://www.gravatar.com/avatar/3e13178365925ae37993bf7b7418b099',
              label: 'Sheldon Callahan'
            }
          }
        },
        description: {
          text:
            '\nThis is an Application card with a view dialog action to open a dialog window\n'
        },
        context: {
          text: 'Stride Documentation / ... / Nodes',
          icon: {
            url: 'https://image.ibb.co/fPPAB5/Stride_White_On_Blue.png',
            label: 'stride'
          }
        }
      }
    }
  ]
}

module.exports = {
  actionCard
}
