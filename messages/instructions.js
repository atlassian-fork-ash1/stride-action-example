//Data structure for sending a dynamic action as a message to open dialog modal
// We're defining a link that has an action that is targeting a dialog that was defined in the app descriptor.
//https://developer.atlassian.com/cloud/stride/apis/document/marks/action/

let instructions = {
  type: 'doc',
  version: 1,
  content: [
    {
      type: 'heading',
      content: [
        {
          type: 'text',
          text: 'Welcome the the Actions Example App '
        }
      ],
      attrs: {
        level: 3
      }
    },
    {
      type: 'paragraph',
      content: [
        {
          type: 'text',
          text: 'You can find the '
        },
        {
          type: 'emoji',
          attrs: {
            shortName: ':bitbucket:'
          }
        },
        {
          type: 'text',
          text: 'Bitbucket Repo:',
          marks: [
            {
              type: 'link',
              attrs: {
                href:
                  'https://bitbucket.org/atlassian/stride-action-example/src/master/',
                id: '413780b0-0c85-4963-9575-13d2a8253379',
                occurrenceKey:
                  'ari:cloud:media:c2975376-5210-47df-b9a4-ebc9d2551f9b',
                collection: 'ffa65a80-8a14-459b-87d4-15b06877533e'
              }
            }
          ]
        },
        {
          type: 'text',
          text: ' here '
        }
      ]
    },
    {
      type: 'rule'
    },
    {
      type: 'paragraph',
      content: [
        {
          type: 'text',
          text: 'These following examples focus on using:'
        }
      ]
    },
    {
      type: 'bulletList',
      content: [
        {
          type: 'listItem',
          content: [
            {
              type: 'paragraph',
              content: [
                {
                  type: 'text',
                  text: 'Input Action',
                  marks: [
                    {
                      type: 'link',
                      attrs: {
                        href:
                          'https://developer.atlassian.com/cloud/stride/apis/modules/chat/inputAction/',
                        title: 'Input Action Docs'
                      }
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          type: 'listItem',
          content: [
            {
              type: 'paragraph',
              content: [
                {
                  type: 'text',
                  text: 'Action Target',
                  marks: [
                    {
                      type: 'link',
                      attrs: {
                        href:
                          'https://developer.atlassian.com/cloud/stride/apis/modules/chat/actionTarget/',
                        title: 'Action Target Docs'
                      }
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          type: 'listItem',
          content: [
            {
              type: 'paragraph',
              content: [
                {
                  type: 'text',
                  text: 'Message Action',
                  marks: [
                    {
                      type: 'link',
                      attrs: {
                        href:
                          'https://developer.atlassian.com/cloud/stride/apis/modules/chat/messageAction/',
                        title: 'Message Action Docs'
                      }
                    }
                  ]
                }
              ]
            },
            {
              type: 'bulletList',
              content: [
                {
                  type: 'listItem',
                  content: [
                    {
                      type: 'paragraph',
                      content: [
                        {
                          type: 'text',
                          text: 'Actions as links in messages Mark Action',
                          marks: [
                            {
                              type: 'link',
                              attrs: {
                                href:
                                  'https://developer.atlassian.com/cloud/stride/apis/document/marks/action/',
                                title: 'Mark Action Docs'
                              }
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          type: 'listItem',
          content: [
            {
              type: 'paragraph',
              content: [
                {
                  type: 'text',
                  text: 'Actions as buttons in Application Cards',
                  marks: [
                    {
                      type: 'link',
                      attrs: {
                        href:
                          'https://developer.atlassian.com/cloud/stride/apis/document/nodes/applicationCard/',
                        title: 'Application Card Docs'
                      }
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          type: 'listItem',
          content: [
            {
              type: 'paragraph',
              content: [
                {
                  type: 'text',
                  text: 'Actions from the Javascrip API',
                  marks: [
                    {
                      type: 'link',
                      attrs: {
                        href:
                          'https://developer.atlassian.com/cloud/stride/apis/jsapi/action/openTarget/',
                        title: 'JavascriptAPI Open Target Docs'
                      }
                    }
                  ]
                }
              ]
            },
            {
              type: 'paragraph',
              content: [
                {
                  type: 'text',
                  text: 'Link to actions overview docs',
                  marks: [
                    {
                      type: 'link',
                      attrs: {
                        href:
                          'https://developer.atlassian.com/cloud/stride/learning/adding-actions/',
                        title: 'Adding Actions Docs'
                      }
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    },
    {
      type: 'panel',
      content: [
        {
          type: 'paragraph',
          content: [
            {
              type: 'text',
              text: 'Examples: ',
              marks: [
                {
                  type: 'strong'
                }
              ]
            },
            {
              type: 'text',
              text: '1. Application Cards: ',
              marks: [
                {
                  type: 'strong'
                }
              ]
            },
            {
              type: 'text',
              text:
                '@mentioning the app will trigger an application card that you can open a dialog from',
              marks: [
                {
                  type: 'em'
                }
              ]
            }
          ]
        }
      ],
      attrs: {
        panelType: 'info'
      }
    },
    {
      type: 'applicationCard',
      attrs: {
        text: 'Application Card',
        title: {
          text: 'Application Card'
        },
        actions: [
          {
            title: 'view dialog',
            target: {
              key: 'app-dialog'
            },
            parameters: {
              param1: 'This is parameters from a Application Card'
            }
          }
        ],
        title: {
          text: 'Sheldon Callahan updated a file: applicationCard.',
          user: {
            icon: {
              url:
                'https://www.gravatar.com/avatar/3e13178365925ae37993bf7b7418b099',
              label: 'Sheldon Callahan'
            }
          }
        },
        collapsible: false,
        textUrl:
          'https://developer.atlassian.com/cloud/stride/apis/document/nodes/applicationCard/',
        background: {
          url: 'https://www.atlassian.com'
        },
        description: {
          text:
            'This is an Application card with a view dialog action to open a dialog window'
        },
        details: [
          {
            title: 'Type',
            text: 'Task',
            icon: {
              url:
                'https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype',
              label: 'Task'
            }
          },
          {
            title: 'User',
            text: 'Joe Blog',
            icon: {
              url:
                'https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype',
              label: 'Task'
            }
          }
        ]
      }
    },
    {
      type: 'panel',
      content: [
        {
          type: 'paragraph',
          content: [
            {
              type: 'text',
              text: 'Examples: 2. Actions as links in messages: ',
              marks: [
                {
                  type: 'strong'
                }
              ]
            },
            {
              type: 'text',
              text: ' type',
              marks: [
                {
                  type: 'em'
                }
              ]
            },
            {
              type: 'text',
              text: ' "action" ',
              marks: [
                {
                  type: 'strong'
                }
              ]
            },
            {
              type: 'text',
              text:
                ' in any message and the app will trigger a link that can open a dialog. You can also type in',
              marks: [
                {
                  type: 'em'
                }
              ]
            },
            {
              type: 'text',
              text: ' "instructions" ',
              marks: [
                {
                  type: 'strong'
                }
              ]
            },
            {
              type: 'text',
              text: 'and get these instructions again',
              marks: [
                {
                  type: 'em'
                }
              ]
            }
          ]
        }
      ],
      attrs: {
        panelType: 'info'
      }
    },
    {
      type: 'paragraph',
      content: [
        {
          type: 'text',
          text: 'This is an action from inside a message to '
        },
        {
          type: 'text',
          text: 'Open a Dialog',
          marks: [
            {
              type: 'action',
              attrs: {
                title: 'view dialog',
                target: {
                  key: 'app-dialog'
                },
                parameters: {
                  param1: 'This is a parameter from from a text link in a message'
                }
              }
            }
          ]
        }
      ]
    },
    {
      type: 'paragraph',
      content: [
        {
          type: 'text',
          text:
            'The Sidebar will have further instructions and you can also open it here: ',
          marks: [
            {
              type: 'strong'
            }
          ]
        },
        {
          type: 'text',
          text: 'Open Sidebar',
          marks: [
            {
              type: 'action',
              attrs: {
                title: 'view sidebar',
                target: {
                  key: 'app-action-openSidebar'
                }
              }
            }
          ]
        }
      ]
    }
  ]
}

module.exports = {
  instructions
}
