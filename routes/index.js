//
// Set up Enviorment Variables
// -----------------------------------------------------------------------------

const CLIENT_ID = process.env.CLIENT_ID
const CLIENT_SECRET = process.env.CLIENT_SECRET
const API_BASE_URL = 'https://api.atlassian.com'

var express = require('express')
var router = express.Router()
const fs = require('fs')
const path = require('path')
const jwt = require('../lib/jwt.js')
// var debug = require('debug')('stride:index-route')
var sendToStride = require('../lib/send-message.js').factory({
  apiBaseUrl: API_BASE_URL,
  clientId: CLIENT_ID,
  clientSecret: CLIENT_SECRET
})

/* GET home page */
router.get('/', function(req, res) {
  res.redirect('/descriptor')
})

//
// Send descriptor
// -----------------------------------------------------------------------------
router.get('/descriptor', (req, res) => {
  let descriptor = JSON.parse(
    fs.readFileSync(path.join(__dirname, '../app-descriptor.json')).toString()
  )
  descriptor.baseUrl = 'https://' + req.headers.host
  res.contentType = 'application/json'
  res.send(descriptor)
  res.end()
})

//
// Install life cycle webhook
// -----------------------------------------------------------------------------
router.post('/installed', jwt.verifyRequest, async (req, res) => {
  // context.cloudId        - the site id that contains the conversation that the app was installed in.
  // context.userId         - the user id that installed the app.
  // context.conversationId - the conversation id that the app was installed in.
  let context = {
    cloudId: req.body.cloudId,
    userId: req.body.userId,
    conversationId: req.body.resourceId
  }

  let {instructions} = await require('../messages/instructions')

  await sendToStride.sendMessage(
    context.cloudId,
    context.conversationId,
    instructions,
    function() {
      //Stop Webhook from sending 3 times by returning 200;
      res.status(200)
    }
  )
  res.status(200).end()
})

//
// Uninstall life cycle webhook
// -----------------------------------------------------------------------------

router.post('/uninstalled', jwt.verifyRequest, (req, res) => {
  let context = {
    cloudId: req.body.cloudId,
    userId: req.body.userId,
    conversationId: req.body.resourceId
  }

  //TODO: Use context to remove the data from your data store
  debug('Uninstalled payload:', context)

  res.status(200).end()
})

// //
// // Serve the html to the modal dialog window on stride
// // -----------------------------------------------------------------------------
router.get('/dialog', (req, res) => {
  res.render('modal', {content: 'Modal Opened from  '})
})

// //
// // Serve the html from a message action to the modal dialog window on stride
// // -----------------------------------------------------------------------------
router.get('/message-dialog', (req, res) => {
  res.render('modal', {content: 'Modal Opened from  '})
})

// // If the bot is mention it will send an applicaiton card back with open dialog
// // action button
// // -----------------------------------------------------------------------------
router.post('/bot-mention', jwt.verifyRequest, (req, res) => {
  let {actionCard} = require('../messages/action-card')
  let {body: {cloudId, conversation}} = req
  sendToStride.sendMessage(cloudId, conversation.id, actionCard, function() {
    //Stop Webhook from sending 3 times by returning 200;
    res.send('200')
  })
})

// //
// // If the word "action" is mentioned in a message this url will be hit and send
// // back a response
// // -----------------------------------------------------------------------------
router.post('/action-message', jwt.verifyRequest, function(
  {body: {cloudId, conversation}},
  res
) {
  // Load Action Message
  let {actionMessage} = require('../messages/action-message')

  // Send Action Message to Stride Conversation
  sendToStride.sendMessage(cloudId, conversation.id, actionMessage, function() {
    //Stop Webhook from sending 3 times by returning 200;

    res.send('200')
  })
})

// //
// // If the word "instructions" is mentioned in a message this url will be hit and send
// // back a response
// // -----------------------------------------------------------------------------
router.post('/instructions', jwt.verifyRequest, function(
  {body: {cloudId, conversation}},
  res
) {
  // Load Instuctions
  let {instructions} = require('../messages/instructions.js')

  // Send Instructions to Stride Conversation
  sendToStride.sendMessage(cloudId, conversation.id, instructions, function() {
    //Stop Webhook from sending 3 times by returning 200;
    res.send('200')
  })
})

// //
// // Sidebar
// // Render Sidebar content
// // -----------------------------------------------------------------------------
router.get('/sidebar', jwt.verifyRequest, (req, res) => {
  res.render('sidebar', {content: 'Sidebar Action'})
})
router.get('/glance/state', (req, res) => {
  res.send({
    label: {
      value: 'Sidebar Action'
    }
  })
})

// //
// // Configuration
// // Render Configuration content
// // -----------------------------------------------------------------------------
router.get('/configuration', (req, res) => {
  res.render('config', {content: 'configuration page'})
})

module.exports = router
